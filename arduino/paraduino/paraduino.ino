
#include<Wire.h>
#include "Joystick.h"

#define mpu_addr 0x68
#define TCAADDR 0x70

// I2C bus channels (0..7)
#define bus_left_brake   2
#define bus_right_brake  3         
#define bus_weight_shift 4

const int16_t roller_radius = 80; // Radius in mm
const int16_t brake_range = 460; // Radius in mm

const int16_t minVal=265;
const int16_t maxVal=402;

int16_t AcX,AcY,xAng,yAng, z;
int16_t offset_left_brake = 0, offset_right_brake = 0;
int16_t z_left_brake, z_right_brake, z_weight_shift;
int16_t MaxBrakeAngle = 359;
int16_t MinBrakeAngle = 1;

Joystick_ Joystick;

void setup(){
  Wire.begin();
  delay(100);
  
  // Prepare MPUs
  prepareMPU(mpu_addr, bus_left_brake);
  prepareMPU(mpu_addr, bus_right_brake);
  prepareMPU(mpu_addr, bus_weight_shift);

  offset_left_brake = GetAngle(mpu_addr, bus_left_brake);
  offset_right_brake = GetAngle(mpu_addr, bus_left_brake);
 
  MaxBrakeAngle = (MaxBrakeAngle/(2*3.14*roller_radius)*brake_range) - 1;
  
  // Set Range Values
  Joystick.setXAxisRange(0, MaxBrakeAngle);
  Joystick.setYAxisRange(0, MaxBrakeAngle);
  Joystick.setZAxisRange(160, 200);
  
  Joystick.begin();
  delay(100);

}
void loop(){

  z_left_brake = GetAngle(mpu_addr, bus_left_brake);
  z_right_brake = -1*(GetAngle(mpu_addr, bus_right_brake) - 360);
  z_weight_shift = GetAngle(mpu_addr, bus_weight_shift);

  z_left_brake = processBrakeAngle(z_left_brake, offset_left_brake);
  z_right_brake = processBrakeAngle(z_right_brake, offset_right_brake);

  Joystick.setXAxis(z_left_brake);
  Joystick.setYAxis(z_right_brake);
  Joystick.setZAxis(z_weight_shift);
    
  delay(100);
}

int GetAngle(byte addr, byte bus){

  tcaselect(bus);
  Wire.beginTransmission(addr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(0x68,14,true);

  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();

  xAng = map(AcX,minVal,maxVal,-90,90);
  yAng = map(AcY,minVal,maxVal,-90,90);

  z = int (RAD_TO_DEG * (atan2(-yAng, -xAng)+PI));
  return z;
}

// enable requested I2C channel (0..7)
void tcaselect(byte i) {
  if (i > 7) return;
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}

void prepareMPU(byte addr, byte bus){
  tcaselect(bus);
  Wire.beginTransmission(addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  delay(100); // Do not remove! Important for setup routine!
}

int16_t processBrakeAngle(int16_t brakeAngle, int16_t offset){

  if(brakeAngle >= offset) { brakeAngle = brakeAngle - offset; }
  else { brakeAngle = offset; }

  if(brakeAngle >= MaxBrakeAngle) brakeAngle = MaxBrakeAngle;
  if(brakeAngle <= MinBrakeAngle + 2) brakeAngle = MinBrakeAngle;

  return brakeAngle;

  }
