# paraduino

paraduino is a realistic DIY game controller for the [Paragliding Sim ](https://evan-burrows.itch.io/paragliding-sim) by Evan Burrows. Many Thanks to Evan! Great work!

*(Of course all paraduino does is acting like a standard game controller. So chances are you can use it with other paragliding sims.)*

Paragliding Sims can be annoying when used with unrealistic controls like a keyboard or a standard joystick. Paraduino helps you building youre own realistic flight controller for paragliding sims. All you need is a piece of wood, four pulleys, some electronic parts (e.g. an Ardunino Micro or Arduino UNO) and some other stuff you'll have lying around.

## How long will it take to build one?
Well, if you have the tools and if you're a little bit technically gifted it'll take about half a day i think. But maybe it's ok to calculate a weekend. Take you're time and see how things are going. It's a hobby, don't care about the time too much!

## So what do i need in detail?

#### Electronic parts
* 1x Arduino Micro (Only the "Micro" and the "UNO" have the needed uC "Atmega 32U")
* 1 TCA9548A breakout boards (I2C switch)
* 3 MPU650 breakout board (Acc/Gyro measurement)
* 3 USB-A to pin boards
* 3 USB-B micro to pin boards
* 1 stripboard
* 3 USB _data_ cables. (USB-A to USB micro)
* 1 loooong USB data cable. This one is for the connection between the Arduino and your PC. (Could be USB-A to USB micro, depends on your Arduino). I was able to get one in 5m length. That's pretty comfortable.

#### Mechanical parts
* 1 wooden plate, approximately 30x130cm
* 2 pulleys with a minimum diameter of 160mm
* 2 pulleys with a diameter between 40 and 160mm
* 1 thin rope/line (roughly 5m). I prefer sailing lines around 3mm.
* 2 Handles (Nearly everything should work, even a simple sling at the end the ropes.)
* 2 Weights around 1kg (Sandbag, Bottle of water, ...)


#### Tools
* Drilling machine or cordless screwdriver
* Some wood drills (10mm, 3mm, maybe some more)
* Soldering iron (You only need to solder standard 2.54mm pins. No SMT stuff, so it won't get hard)
* Well, and some other stuff you'll have lying around. Screwdriver, knife, scissors


## How to build ##

To be continued...

##### Soldering the electronic parts

##### Mechanical part

##### Last steps

